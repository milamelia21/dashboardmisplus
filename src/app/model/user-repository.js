class UserRepository {
    // Nanti kalau sudah ada API login !=null
    get isLoggedIn() {
        return sessionStorage.getItem('token') == null
    }

    get token() {
        return sessionStorage.getItem('token')
    }

    get name() {
        return sessionStorage.getItem('name')
    }

    get phone() {
        return sessionStorage.getItem('phone')
    }

    get NamaCompany() {
        return sessionStorage.getItem('NamaCompany')
    }

    get level() {
        return sessionStorage.getItem('level')
    }

    async picture() {
        var data = await apiProvider.user(this.phone);
        return data[0].foto_profile;
    }

    loggedOut() {
        sessionStorage.removeItem('token');
        Navigator.push('/');
    }
}