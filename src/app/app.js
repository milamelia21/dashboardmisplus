var route;
var apiProvider = new ApiProvider();
var userRepository = new UserRepository();
const routes = [
    new Route('Login', '/', '/login', false),
    new Route('Register', '/register', '/register', false),
    new Route('Dashboard', '/dashboard', '/dashboard', true),
    new Route('Print', '/print/:menuPrint/:data', '/print', true),
];
var INITIAL_DASHBOARD_PAGE = 'menu/main-navigation/dashboard/analytics/analytics';
router(routes);