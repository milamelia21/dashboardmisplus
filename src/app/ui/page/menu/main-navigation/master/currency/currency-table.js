// initial state
var currency;
async function setTableCurrency() {
    await $('#table-currency').tableSetDataFromAPI(async () => await apiProvider.MasterCurrency(),
        function (item) {
            return /*html*/ `<tr>
                            <td>${item.KodeCurrency}</td>
                            <td>${item.NamaCurrency}</td>
                            <td>
                                <button class="btn btn-danger btn-sm btn-icon-only" onclick="deleteFunction(this)" data-toggle="tooltip" title="Delete" type="button" data-json='${JSON.stringify(item)}''>
                                    <i class="material-icons-outlined">delete</i>
                                </button>
                            </td>
                        </tr>`
        })
    $('#table-currency').DataTable({
        responsive: true,
        "scrollX": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
}
setTableCurrency();

// events
$('#create-currency').click(function (e) {
    currency = null;
    $('#page-content').load('src/app/ui/page/menu/main-navigation/master/currency/currency-create.html')
});


async function deleteFunction(e) {
    if (await MessageBox.confirm()) {
        currency = JSON.parse(e.dataset.json);
        var result = await apiProvider.MasterCurrencyDelete(
            currency.RecID,
            currency.KodeCurrency
        );
        if (result)
            $('#page-content').load('src/app/ui/page/menu/main-navigation/master/currency/currency-table.html')
    }
    
}
