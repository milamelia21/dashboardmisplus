$('#page-title').titlePage({
    icon: 'bookmark',
    iconColor: ['#1476be', '#8fd3f4'],
    title: ' Currency',
    subtitle: 'Currency.'
});

$('#page-content').load('src/app/ui/page/menu/main-navigation/master/currency/currency-table.html')