// initial state



// event
$('#input-currency').submit(async function (e) {
    e.preventDefault();
    var isValid = true;
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
        e.stopPropagation();
        isValid = false;
    }

    form.classList.add('was-validated');
    if (isValid) {
        await apiProvider.MasterCurrencyCreate(
            form['kodeCurrency'].value,
            form['namaCurrency'].value,
        )
        $('#back').trigger('click');
    }
});

$('#form-cancel, #back').click(function (e) {
    e.preventDefault();
    $('#page-content').load('src/app/ui/page/menu/main-navigation/master/currency/currency-table.html')
});