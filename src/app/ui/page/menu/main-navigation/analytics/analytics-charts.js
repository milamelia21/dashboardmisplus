// ANCHOR Nominal
function nominal(nilai) {
    if (nilai >= 1000000000) {
        var hasil = (nilai / 1000000000).toFixed(2) + " M"
        return hasil
    } else if (nilai >= 1000000) {
        var hasil = (nilai / 1000000).toFixed(2) + " JT"
        return hasil
    } else if (nilai >= 1000) {
        var hasil = (nilai / 1000).toFixed(2) + " RB"
        return hasil
    } else if (nilai == 0) {
        return "0.00"
    } else if (nilai <= -1000000000) {
        var hasil = (nilai / 1000000000).toFixed(2) + " M"
        return hasil
    } else if (nilai <= -1000000) {
        var hasil = (nilai / 1000000).toFixed(2) + " JT"
        return hasil
    } else if (nilai <= -1000) {
        var hasil = (nilai / 1000).toFixed(2) + " RB"
        return hasil
    } else {
        return nilai
    }
}

// ANCHOR Show Loading
function showLoading() {
    Swal.fire({
        title: 'PLEASE WAIT',
        text: 'processing your request',
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        },
    });
}

function getMonth(periode) {
    if (periode == "01") {
        return "Jan"
    } else if (periode == "02") {
        return "Feb"
    } else if (periode == "03") {
        return "Mar"
    } else if (periode == "04") {
        return "Apr"
    } else if (periode == "05") {
        return "May"
    } else if (periode == "06") {
        return "Jun"
    } else if (periode == "07") {
        return "Jul"
    } else if (periode == "08") {
        return "Aug"
    } else if (periode == "09") {
        return "Sep"
    } else if (periode == "10") {
        return "Oct"
    } else if (periode == "11") {
        return "Nov"
    } else if (periode == "12") {
        return "Dec"
    }
}

// ANCHOR End Loading
function endLoading() {
    Swal.close();
}

// ANCHOR Chart Config
function chartConfig(chart) {
    chart.logo.height = -150
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
    chart.legend = new am4charts.Legend();

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "month";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.grid.template.location = 0.5;
    categoryAxis.startLocation = 0.3;
    categoryAxis.endLocation = 0.7;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.numberFormatter = new am4core.NumberFormatter();
    valueAxis.numberFormatter.numberFormat = '#a';
    valueAxis.numberFormatter.bigNumberPrefixes = [{
        "number": 1e+3,
        "suffix": " RB"
    },
    {
        "number": 1e+6,
        "suffix": " JT"
    },
    {
        "number": 1e+9,
        "suffix": " M"
    }
    ];
}

// ANCHOR Chart Series
function chartSeries(chart, color, value, valueNominal, dept) {
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = value;
    series.dataFields.categoryX = "month";
    if (dept != undefined) {
        series.name = dept + " " + new Date().getFullYear().toString();
        if (dept.substring(0, 6) == "Target") {
            series.strokeWidth = 2;
            series.strokeDasharray = "5";
        } else {
            series.strokeWidth = 3;
        }
    } else {
        series.name = new Date().getFullYear().toString();
        series.strokeWidth = 3;
    }
    series.tensionX = 0.7;
    series.bullets.push(new am4charts.CircleBullet());
    series.tooltipText = "[bold]{" + valueNominal + "}[/]";
    series.tooltip.label.interactionsEnabled = true;
    series.tooltip.keepTargetHover = true;
    series.tooltip.pointerOrientation = "vertical";
    series.tooltip.label.textAlign = "middle";
    series.tooltip.label.textValign = "middle";
    series.stroke = am4core.color(color)
    series.fill = am4core.color(color)
}

// TODO Sales Ethical
async function getLine() {
    var data = await apiProvider.ChartLine()
    if (data != null) {
        if (data.length > 0) {
            var chartData = []
            var chart = am4core.create('LineChart', am4charts.XYChart);
            chartConfig(chart)
            chartSeries(chart, "#66CDD9", "value", "valueNominal")

            if (data.length > 0) {
                for (i = 0; i < 12; i++) {
                    chartData[i] = {
                        "month": getMonth((data[i].Periode).substring(4, 6)),
                        "value": data[i].VALUE,
                        "valueNominal": nominal(data[i].VALUE),
                    }
                }
                chart.data = chartData
            }
        } else {
            $("#LineChart").empty()
            $("#LineChart").append(`<div class="text-center">Data Tidak Ditemukan</div>`)
        }
    } else {
        $("#LineChart").empty()
        $("#LineChart").append(`<div class="text-center">Data Tidak Ditemukan</div>`)
    }
}

async function getSpider() {
    var data = await apiProvider.ChartSpider()
    var chartData = []
    data.forEach(function (dataChart, index) {
        chartData.push({
            "jual": dataChart.QtyJual,
            "stock": dataChart.QtyStock,
            "product": dataChart.namaproduk
        })
    })
    var chart = am4core.create("SpiderChart", am4charts.RadarChart);
    chart.logo.height = -150
    chart.legend = new am4charts.Legend();

    /* Add data */
    chart.data = chartData;

    /* Create axes */
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "product";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.gridType = "polygons"
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 1;

    /* Create and configure series */
    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "jual";
    series.dataFields.categoryX = "product";
    series.tooltipText = "[bold]{jual}[/]";
    series.tooltip.label.interactionsEnabled = true;
    series.tooltip.keepTargetHover = true;
    series.tooltip.pointerOrientation = "vertical";
    series.tooltip.label.textAlign = "middle";
    series.tooltip.label.textValign = "middle";
    series.name = "Jual";
    series.strokeWidth = 2;
    series.stroke = am4core.color('#F55D44')
    series.fill = am4core.color('#F55D44')


    var series2 = chart.series.push(new am4charts.RadarSeries());
    series2.dataFields.valueY = "stock";
    series2.dataFields.categoryX = "product";
    series2.tooltipText = "[bold]{stock}[/]";
    series2.tooltip.label.interactionsEnabled = true;
    series2.tooltip.keepTargetHover = true;
    series2.tooltip.pointerOrientation = "vertical";
    series2.tooltip.label.textAlign = "middle";
    series2.tooltip.label.textValign = "middle";
    series2.name = "Stock";
    series2.strokeWidth = 2;
    series2.stroke = am4core.color('#FFC848')
    series2.fill = am4core.color('#FFC848')

    chart.cursor = new am4charts.RadarCursor();
}

async function getHeader() {
    var waitingPay = await apiProvider.waitingPay()
    var waitingFA = await apiProvider.waitingFA()
    var waitingShip = await apiProvider.waitingShip()
    var waitingReceive = await apiProvider.waitingReceive()
    $('#waitingPay').empty()
    $('#waitingFA').empty()
    $('#waitingShip').empty()
    $('#waitingReceive').empty()
    if (waitingPay.data[0].WaitingPay == null) {
        $('#waitingPay').append(`<strong>0</strong>`)
    } else {
        $('#waitingPay').append(`<strong>${waitingPay.data[0].WaitingPay}</strong>`)
    }
    if (waitingFA.data[0].WaitingFA == null) {
        $('#waitingFA').append(`<strong>0</strong>`)
    } else {
        $('#waitingFA').append(`<strong>${waitingFA.data[0].WaitingFA}</strong>`)
    }
    if (waitingShip.data[0].WaitingShip == null) {
        $('#waitingShip').append(`<strong>0</strong>`)
    } else {
        $('#waitingShip').append(`<strong>${waitingShip.data[0].WaitingShip}</strong>`)
    }
    if (waitingReceive.data[0].WaitingReceive == null) {
        $('#waitingReceive').append(`<strong>0</strong>`)
    } else {
        $('#waitingReceive').append(`<strong>${waitingReceive.data[0].WaitingReceive}</strong>`)
    }
}

getHeader()
getLine()
getSpider()

$('#waiting-payment').click(function (e) {
    var route = '#/dashboard/main-navigation/transaction/confirm-so'
    location.href = route;
})
$('#waiting-fa').click(function (e) {
    var route = '#/dashboard/main-navigation/transaction/confirm-payment'
    location.href = route;
})
$('#waiting-shipping').click(function (e) {
    var route = '#/dashboard/main-navigation/transaction/confirm-fa'
    window.history.pushState('page2', 'Title', '#/dashboard/analytics/analytics');
    location.href = route;
})
$('#waiting-receive').click(function (e) {
    var route = '#/dashboard/main-navigation/transaction/confirm-shipping'
    location.href = route;
})