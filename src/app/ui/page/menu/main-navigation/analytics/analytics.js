if (!userRepository.isLoggedIn) {
    userRepository.loggedOut()
} else {
    $('#page-title').titlePage({
        icon: 'tag_faces',
        iconColor: ['#1476be', '#8fd3f4'],
        title: 'Hello, ' + userRepository.name,
        subtitle: '“Don’t find customers for your products, find products for your customers.” – Seth Godin'
    });
}
$('#page-content').load('src/app/ui/page/menu/main-navigation/dashboard/analytics/analytics-charts.html')