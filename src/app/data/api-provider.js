class ApiProvider extends BaseApi {
    async login(email, password) {
        var url = `${super.baseUrl}login`;
        var auth = btoa('PWA' + ',' + email + ':' + password);
        return await super.request(url, {
            headers: {
                'Authorization': `Basic ${auth}`
            },
            method: 'GET',
            showProgress: true,
        });
    }
    async loginas(phone_admin, phone_dev) {
        var url = `${super.baseUrl}loginas?sPhone=${phone_admin}&sPhoneDev=${phone_dev}`;
        var response = await super.request(
            url, {
            headers: {
                'Authorization': `Bearer ${userRepository.token}`,
            },
            method: 'GET',
            showProgress: true,
        },
        );
        return response;
    }

    async user(phoneNumber) {
        var url = `${super.baseUrl}user?phone_user=${phoneNumber}`;
        return (await super.request(
            url, {
            headers: {
                'Authorization': `Bearer ${userRepository.token}`,
            },
            method: 'GET',
        }
        )).data;
    }

    async MasterCurrency() {
        var url = `${super.baseUrl}MasterCurrency`;
        return (await super.request(
            url, {
            headers: {
                'Authorization': `Bearer ${userRepository.token}`,
            },
            method: 'GET',
            showProgress: true,
        }
        )).data;
    }

    async MasterCurrencyCreate(kodeCurrency, namaCurrency) {
        var url = `${super.baseUrl}MasterCurrency`
        return await super.request(
            url, {
            headers: {
                'Authorization': `Bearer ${userRepository.token}`,
            },
            method: 'POST',
            showProgress: true,
            body: {
                KodeCurrency: kodeCurrency,
                NamaCurrency: namaCurrency,
            }
        }
        )
    }

    async MasterCurrencyDelete(recID, kodeCurrency) {
        var url = `${super.baseUrl}MasterCurrency`
        return await super.request(
            url, {
            headers: {
                'Authorization': `Bearer ${userRepository.token}`,
            },
            method: 'DELETE',
            showProgress: true,
            body: {
                RecID: recID,
                KodeCurrency: kodeCurrency,
            }
        }
        )
    }

}