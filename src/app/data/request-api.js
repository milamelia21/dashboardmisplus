/**
 * @example
 * requestWithToken({
    showMessageFailure: true,
    showSuccessAlert: true,
    showLoading: true,
    method: 'DELETE',
    api: 'menu',
    body: {
        sNamaSubMenu: subMenu,
        sGroupMenu: groupMenu,
        sNamaMenu: menu,
        sPathUrl: path,
        sIcon: icon,
    }
});
*/
async function requestWithToken(options) {
    var statusCode;
    if (options.showLoading)
        Swal.fire({
            title: 'Please Wait !',
            text: 'processing your request',
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });
    var formdata = typeof options.body !== 'undefined' && ['POST', 'PUT', 'DELETE'].includes(options.method) ? new FormData() : null;
    var param = '';
    if (['POST', 'PUT', 'DELETE'].includes(options.method)) {
        for (var key in options.body) {
            formdata.append(key, options.body[key]);
        }
    }
    if (options.method == 'GET') {
        param = new URLSearchParams(options.body).toString();
        if (param != '')
            param = '?' + param
    }
    var requestOptions = {
        method: options.method,
        headers: {
            'Authorization': 'Bearer ' + Auth.token()
        },
        body: formdata,
    };
    try {
        var response = await fetch(hostAPI + options.api + param, requestOptions);
        var data = await response.json();
        statusCode = response.status;
        if (options.showLoading)
            swal.close();
        if (statusCode == 200) {
            if (options.showSuccessAlert)
                MessageBox.flashSuccess();
            return data;
        } else {
            MessageBox.flashError(options.showMessageFailure ? data.note : null);
        }
    } catch (error) {
        if (statusCode == 400)
            Auth.loggedOut()
        MessageBox.flashError();
        return null
    }
}

async function requestPhotoWithToken(nipKaryawan, dataImage, dataPhoto) {
    var photoToken;
    var loginformdata = new FormData();
    loginformdata.append('username', '2180840');
    loginformdata.append('password', '123456');
    loginformdata.append('company', 'vneu');

    var loginrequestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
        },
        body: loginformdata,
    }

    try {
        var response = await fetch('http://103.106.145.14:8080/login', loginrequestOptions);
        statusCode = response.status;
        var data = await response.json();
        photoToken = data.access_token
    } catch (error) {
        console.log(error)
    }

    var formdata = new FormData();
    formdata.append('username', nipKaryawan);
    formdata.append('photo', dataPhoto, 'image.png');

    var requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + photoToken
        },
        body: formdata,
    }

    try {
        var response = await fetch('http://103.106.145.14:8080/registerphoto', requestOptions);
        statusCode = response.status;
        var data = await response.json();
        console.log(data);
        await TakePhoto.get(
            dataImage,
            data.message.face_detected,
            data.message.face_qty
        )
        return data;
    } catch (error) {
        console.log(error)
        MessageBox.flashError();
    }
}