class BaseApi {
    constructor() {
        this.TIMEOUT_SECONDS = 60;
        this.BASE_URL = '';
    }

    get baseUrl() {
        return this.BASE_URL;
    }

    async request(url, options) {
        let result;
        try {
            if (options.showProgress)
                Swal.fire({
                    title: 'Please Wait !',
                    text: 'processing your request',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                });
            result = await $.ajax({
                url: url,
                type: options.method,
                dataType: 'json',
                processData: false,
                contentType: false,
                timeout: this.TIMEOUT_SECONDS * 1000,
                headers: options.headers,
                data: toFormData(options.body),
            });
            if (options.showProgress) swal.close();
            return result;
        } catch (error) {
            if (options.showProgress) swal.close();
            if (error.status == 400) {
                if (error.responseText.trim() == 'Token is expired') userRepository.loggedOut();
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: error.responseText,
                    showConfirmButton: false,
                    timer: 5000
                })

            } else {
                if (result) {
                    MessageBox.flashError("Error " + result['note'], "center", 1000)
                }
            }
        }
    }
}