class MessageBox {
    static flashError(message, pos, time) {
        Swal.fire({
            position: pos || 'top-end',
            icon: 'error',
            title: message || 'Oops! Something went wrong',
            showConfirmButton: false,
            timer: time || 5000
        })
    }

    static flashSuccess(text, pos, time) {
        Swal.fire({
            position: pos || 'top-end',
            icon: 'success',
            title: text || 'Successfully Submit',
            showConfirmButton: false,
            timer: time || 1500
        })
    }

    static async confirm(text) {
        var result = await Swal.fire({
            title: 'Are you sure?',
            text: text || "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        return result.value;
    }

    static async ubahstatus(text) {
        var result = await Swal.fire({
            title: 'Are you sure?',
            text: text || "Product Request Already Confirmed!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        return result.value;
    }

    static async textArea(text) {
        return await Swal.fire({
            title: text,
            input: 'textarea',
            inputPlaceholder: 'Type your message here...',
            inputAttributes: {
                'aria-label': 'Type your message here'
            },
            showCancelButton: true,
            cancelButtonColor: '#d33',
        })
    }

    static async textbox(text) {
        return await Swal.fire({
            title: text,
            input: 'text',
            inputPlaceholder: 'Type your message here...',
            inputAttributes: {
                'aria-label': 'Type your message here'
            },
            showCancelButton: true,
            cancelButtonColor: '#d33',
        })
    }
}