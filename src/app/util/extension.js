jQuery.fn.extend({
    comboboxSearch: function () {
        $(this).each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    },
    comboboxSearchCustom: function () {
        $(this).each(function () {
            $(this).select2({
                tags: true,
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    },
    setOptionsFromAPI: async function (option) {
        var data = await option.api();
        const select = $(this);
        select.empty();
        select.append(`<option></option>`);
        if (data != undefined) {
            if (data.length == 0 && option.nodataValue != undefined) {
                select.append(`<option value="${option.nodataValue}" selected>${option.nodataValue}</option>`);
            } else {
                data.forEach(function (element) {
                    var dataOption = '';
                    if (option.data != undefined) {
                        var data = option.data(element)
                        for (var key in data) {
                            dataOption += `data-${key}="${data[key]}" `
                        }
                    }
                    if (element[option.value] == option.selected) {
                        select.append(`<option ${dataOption} value="${element[option.value]}" selected>${element[option.text]}</option>`);
                    } else if (option.value2 != null) {
                        select.append(`<option ${dataOption} value="${element[option.value]}" selected>${element[option.text]} (${element[option.value2]} bulan)</option>`);
                    } else {
                        select.append(`<option ${dataOption} value="${element[option.value]}">${element[option.text]}</option>`);
                    }
                })
            }
        } else if (option.nodataValue != undefined) {
            select.append(`<option value="${option.nodataValue}" selected>${option.nodataValue}</option>`);
        }
    },

    setRajaOngkirFromAPI: async function (option) {
        var data = await option.api();
        var results = (JSON.parse(data)).rajaongkir.results
        const select = $(this);
        select.empty();
        select.append(`<option></option>`);
        if (results != undefined) {
            if (results.length == 0 && option.nodataValue != undefined) {
                select.append(`<option value="${option.nodataValue}" selected>${option.nodataValue}</option>`);
            } else {
                results.forEach(function (element) {
                    var dataOption = '';
                    if (option.data != undefined) {
                        var data = option.data(element)
                        for (var key in data) {
                            dataOption += `data-${key}="${data[key]}" `
                        }
                    }
                    if (element[option.value] == option.selected) {
                        select.append(`<option ${dataOption} value="${element[option.value]}" selected>${element[option.text]}</option>`);
                    } else {
                        select.append(`<option ${dataOption} value="${element[option.value]}">${element[option.text]}</option>`);
                    }
                })
            }
        } else if (option.nodataValue != undefined) {
            select.append(`<option value="${option.nodataValue}" selected>${option.nodataValue}</option>`);
        }
    },
    autocompleteOff: function () {
        $(this).attr('autocomplete', 'off')
    },
    titlePage: function (options) {
        $(this).replaceWith(`
            <div class="app-page-title">
                <div class="app-page-title-heading">
                    <div class="app-page-title-icon">
                        <i class="material-icons-outlined icon-gradient" style="background-image: linear-gradient(120deg, ${options.iconColor[0]} 0%, ${options.iconColor[1]} 100%) !important">
                        ${options.icon}
                        </i>
                    </div>
                    <div>
                        ${options.title}
                        <div class="page-title-subheading">${options.subtitle}</div>
                    </div>
                </div>
            </div>
        `);
    },
    topbar: function (options) {
        $(this).load('/src/app/ui/component/topbar/topbar.html', function () {
            $(this).find('.user-info-heading').text(options.user.title);
            $(this).find('.user-info-subheading').text(options.user.subtitle);
            $(this).find('#user-picture').attr('src', options.user.picture);
            $(this).children(':first').unwrap();
        })
    },
    tableGettingDataIndicator: function () {
        var tableColumntLenght = $(this).find('thead tr:eq(0) th').length;
        $(this).find('tbody').empty();
        $(this).find('tbody').append(
            `<tr>
                <td colspan="${tableColumntLenght}">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            Please Wait...
                        </div>
                    </div>
                </td>
            </tr>`
        );
    },
    tableNoDataIndicator: function () {
        var tableColumntLenght = $(this).find('thead tr:eq(0) th').length;
        $(this).find('tbody').empty();
        $(this).find('tbody').append(
            `<tr>
                <td colspan="${tableColumntLenght}">
                    No data available in table
                </td>
            </tr>`
        );
    },
    tableSetDataFromAPI: async function (api, builder) {
        $(this).tableGettingDataIndicator();
        var data = await api();
        console.log(data)
        if (data == null)
            return;
        if (data.length == 0)
            $(this).tableNoDataIndicator();
        $(this).find('tbody').empty();
        var tableRowHTML = data.map(builder);
        $(this).find('tbody').append(tableRowHTML.join());
    },
    divSetDataFromAPI: async function (api, builder) {
        var data = await api();
        if (data == null)
            return;
        if (data.length == 0)
            $(this).tableNoDataIndicator();
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join());
    },
    sidebar: function (initialPage, menu) {
        var menuHTML = '';
        menu.forEach(function (menu) {
            menuHTML += `<div class="vsm-header">${menu.group}</div>`
            var menuGroup = menu.group
            menu.menu.forEach(function (menu) {
                if (menu.name != null) {
                    var collapseId = `collapse` + menuGroup + `${menu.name}`;
                    var subMenuHTML = '';
                    if (menu.submenu.length > 0) {
                        menu.submenu.forEach(function (menu) {
                            subMenuHTML +=
                                `<div class="vsm-item">
                                        <a class="vsm-link route" data-route="${menu.route}">
                                            <span>${menu.name}</span>
                                        </a>
                                    </div>`
                        });
                    }
                    menuHTML +=
                        `<button class="vsm-link route collapsed" data-route="${menu.route}" data-toggle="collapse" data-target="#${collapseId}" aria-expanded="false" aria-controls="${collapseId}">
                                <i class="vsm-icon material-icons-outlined">
                                    ${menu.icon}
                                </i>
                                <span class="vsm-title">${menu.name}</span>`
                    if (menu.submenu.length > 0) {
                        menuHTML += `<i class="vsm-arrow"></i>
                            </button>
                            <div class="vsm-dropdown collapse" id="${collapseId}" data-parent="#vsm-accordion">
                                <div class="vsm-list">${subMenuHTML}</div>
                            </div>`
                    } else {
                        menuHTML += `</button>`
                    }

                }

            });
        });
        $(this).load('/src/app/ui/component/sidebar/sidebar.html', function () {
            $(this).find('#menu').replaceWith(
                `<div class="v-sidebar-menu accordion" id="vsm-accordion">
                   ${menuHTML}
                </div>`
            );
            // initial content
            $('#content').load(`src/app/ui/page/${initialPage}.html`);
            $('.route').click(function (e) {
                if ($(this).attr('data-route') != '') {
                    e.preventDefault();
                    $('.route').removeClass('active-item');
                    $(this).addClass('active-item');
                    const path = $(this).attr('data-route');
                    window.history.pushState('page2', 'Title', `/#/dashboard/${path}`);
                    $('#content').load(`src/app/ui/page/menu/${path}/${path.split('/').last()}.html`, function (response, status, xhr) {
                        if (status == "error") {
                            var msg = "Sorry but there was an error: ";
                            $("#content").html(msg + xhr.status + " " + xhr.statusText);
                        };
                    })
                }

            });
            $(this).children(':first').unwrap();
        })
    },
});

Array.prototype.last = function () {
    if (this.length == 0)
        return null
    else
        return this[this.length - 1];
};

String.prototype.toIDR = function () {
    return 'Rp' + this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

function toIDR(angka, prefix){
    if (angka != null || angka != undefined) {
        var number_string = angka.toString().replace(/[^,\d]/g, ''),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp' + rupiah : '');
    } else {
        return 'Rp0';
    }
    
}

var toFormData = function (body) {
    var formdata = new FormData();
    for (var key in body) {
        formdata.append(key, body[key]);
    }
    return formdata;
}

// String.prototype.dateFormat = function () {
//     return moment(this).format('L');
// };
// String.prototype.ifEmpty = function () {
//     return this == "" ? '-' : this;
// };
// String.prototype.isEmpty = function () {
//     return this == "";
// }
// String.prototype.isNotEmpty = function () {
//     return this != "";
// }
// String.prototype.capitalizeFirstLetterOfEachWord = function () {
//     return this.split(/ /g).map(val => val[0].toUpperCase() + val.slice(1)).join(' ')
// }