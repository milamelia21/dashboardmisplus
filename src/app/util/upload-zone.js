function UploadZone(option) {
    return new Dropzone(".dropzone", {
        url: '/file/post',
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: true,
        maxFiles: 5,
        maxfilesexceeded: function (file) {
            this.removeAllFiles();
            this.addFile(file);
        },
    })
}