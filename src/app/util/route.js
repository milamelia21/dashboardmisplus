class Route {
    constructor(title, path, view, auth) {
        this.title = title;
        this.path = path;
        this.view = view;
        this.auth = auth;
    }

    setProps(newProps) {
        this.props = newProps;
    }

    renderView() {
        return this.view(this.props);
    }
}
