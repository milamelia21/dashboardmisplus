class Router {
    constructor(routes = [], renderNode) {
        this.routes = routes;
        this.renderNode = renderNode;
        this.navigate(location.pathname + location.hash);
    }

    addRoutes(routes) {
        this.routes = [...this.routes, ...routes];
    }

    match(route, requestPath) {
        let paramNames = [];
        let regexPath = route.path.replace(/([:*])(\w+)/g, (full, colon, name) => {
            paramNames.push(name);
            return '([^\/]+)';
        }) + '(?:\/|$)'
        let params = {};
        let routeMatch = requestPath.match(new RegExp(regexPath));
        if (routeMatch !== null) {
            params = routeMatch
                .slice(1)
                .reduce((params, value, index) => {
                    if (params === null) params = {};
                    params[paramNames[index]] = value;
                    return params;
                }, null);
        }
        route.setProps(params);
        return routeMatch;
    }

    navigate(path) {
        route = this.routes.filter(route => this.match(route, path))[0];
        let title = 'Dashborad MisPlus';
        console.log('route', route)
        if (!route) {
            title += 'Page not found'
            this.renderNode.innerHTML = "404! Page not found";
        } else {
            const pathSplited = path.split('/');
            if (pathSplited.length == 6)
                INITIAL_DASHBOARD_PAGE = path.replace('/#/dashboard', '/menu') + '/' + pathSplited[5];
            else if (pathSplited.length == 5)
                INITIAL_DASHBOARD_PAGE = path.replace('/dashboard', '/menu') + '/' + pathSplited[4];

            title += route.title;
            window.location.href = path.search('/#') === -1 ? '#' + path : path;
            $(this.renderNode).load(`/src/app/ui/page${route.view}${route.view}.html`);
        }
        document.title = title;
    }
}

var router = function (routes) {
    const router = new Router(routes, document.getElementsByTagName('app'));
    document.addEventListener('DOMContentLoaded', e => {
        document.querySelectorAll('[route]')
            .forEach(route => route.addEventListener('click', e => {
                e.preventDefault();
                router.navigate(e.target.getAttribute('route'));
            }, false));
    });
    window.addEventListener('hashchange', e => router.navigate(e.target.location.hash.substr(1)));
}

class Navigator {
    static push(path) {
        location.href = "/#" + path
    }

    static back() {
        history.back()
    }
}